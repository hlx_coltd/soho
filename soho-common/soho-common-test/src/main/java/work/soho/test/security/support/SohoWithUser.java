package work.soho.test.security.support;

import org.springframework.core.annotation.AliasFor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.test.context.support.TestExecutionEvent;
import org.springframework.security.test.context.support.WithSecurityContext;
import org.springframework.test.context.TestContext;

import java.lang.annotation.*;

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@WithSecurityContext(factory = SohoWithMockUserSecurityContextFactory.class)
public @interface SohoWithUser {
    /**
     * 用户ID
     *
     * @return
     */
    @AliasFor("id")
    long value() default 1l;

    /**
     * 测试用用户名
     *
     * @return
     */
    String username() default "test";

    /**
     * 用户ID
     *
     * @return
     */
    @AliasFor("value")
    long id() default 1l;

    /**
     * 认证角色
     *
     * @return
     */
    String role() default "admin";

    /**
     * The password to be used. The default is "password".
     * @return
     */
    String password() default "password";

    /**
     * Determines when the {@link SecurityContext} is setup. The default is before
     * {@link TestExecutionEvent#TEST_METHOD} which occurs during
     * {@link org.springframework.test.context.TestExecutionListener#beforeTestMethod(TestContext)}
     * @return the {@link TestExecutionEvent} to initialize before
     * @since 5.1
     */
    @AliasFor(annotation = WithSecurityContext.class)
    TestExecutionEvent setupBefore() default TestExecutionEvent.TEST_METHOD;
}
