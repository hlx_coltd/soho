# SOHO文档

本文档为技术开发文档， 使用前建议花十来分钟过一遍能减少很多麻烦。

- [相关开发规范](project-standard.md)
- [文件上传](upload.md)
- [Excel操作](excel.md)
- [分布式锁](lock.md)
- [验证码](captcha.md)
- [鉴权](security.md)
- [短信](sms.md)
- [延时队列](delayed-queue.md)
- [excel](excel.md)
- [项目鉴权](security.md)

模块相关文档

- [文件服务](uploadFile.md)
- [邮件服务](email-service.md)
- [短信服务](sms-service.md)
- [系统配置服务](sys-config.md)
- [单元测试服务](test.md)
