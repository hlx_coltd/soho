单元测试
=======

模块单元测试依赖
--------------

        <dependency>
            <groupId>work.soho</groupId>
            <artifactId>soho-common-test</artifactId>
            <version>1.0-SNAPSHOT</version>
            <scope>test</scope>
        </dependency>

认证：

    @SohoWithUser(id = 6, username = "197489090675871745",  role = "chat")
