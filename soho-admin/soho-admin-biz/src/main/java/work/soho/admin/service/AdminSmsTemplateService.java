package work.soho.admin.service;

import work.soho.admin.domain.AdminSmsTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AdminSmsTemplateService extends IService<AdminSmsTemplate> {

}