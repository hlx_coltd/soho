package work.soho.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.admin.domain.AdminRelease;

public interface AdminReleaseMapper extends BaseMapper<AdminRelease> {

}