package work.soho.admin.service;

import work.soho.admin.domain.AdminUserLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AdminUserLoginLogService extends IService<AdminUserLoginLog> {

}