package work.soho.admin.service;

import work.soho.admin.domain.AdminRelease;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AdminReleaseService extends IService<AdminRelease> {

}