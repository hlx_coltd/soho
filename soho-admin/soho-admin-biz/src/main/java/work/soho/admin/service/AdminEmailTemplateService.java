package work.soho.admin.service;

import work.soho.admin.domain.AdminEmailTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AdminEmailTemplateService extends IService<AdminEmailTemplate> {

}