package work.soho.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.admin.domain.AdminUserLoginLog;

public interface AdminUserLoginLogMapper extends BaseMapper<AdminUserLoginLog> {

}