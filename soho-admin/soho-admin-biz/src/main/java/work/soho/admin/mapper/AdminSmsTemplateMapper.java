package work.soho.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.admin.domain.AdminSmsTemplate;

public interface AdminSmsTemplateMapper extends BaseMapper<AdminSmsTemplate> {

}