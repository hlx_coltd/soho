package work.soho.admin.controller;

import java.time.LocalDateTime;
import work.soho.common.core.util.PageUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import java.util.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import work.soho.common.core.util.StringUtils;
import com.github.pagehelper.PageSerializable;
import work.soho.common.core.result.R;
import work.soho.api.admin.annotation.Node;
import work.soho.admin.domain.AdminSmsTemplate;
import work.soho.admin.service.AdminSmsTemplateService;
import java.util.ArrayList;
import java.util.HashMap;
import work.soho.api.admin.vo.OptionVo;
import work.soho.api.admin.request.BetweenCreatedTimeRequest;
import java.util.stream.Collectors;
import work.soho.api.admin.vo.TreeNodeVo;
/**
 * 短信模板Controller
 *
 * @author fang
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/adminSmsTemplate" )
public class AdminSmsTemplateController {

    private final AdminSmsTemplateService adminSmsTemplateService;

    /**
     * 查询短信模板列表
     */
    @GetMapping("/list")
    @Node(value = "adminSmsTemplate::list", name = "短信模板;;列表")
    public R<PageSerializable<AdminSmsTemplate>> list(AdminSmsTemplate adminSmsTemplate, BetweenCreatedTimeRequest betweenCreatedTimeRequest)
    {
        PageUtils.startPage();
        LambdaQueryWrapper<AdminSmsTemplate> lqw = new LambdaQueryWrapper<AdminSmsTemplate>();
        lqw.eq(adminSmsTemplate.getId() != null, AdminSmsTemplate::getId ,adminSmsTemplate.getId());
        lqw.like(StringUtils.isNotBlank(adminSmsTemplate.getAdapterName()),AdminSmsTemplate::getAdapterName ,adminSmsTemplate.getAdapterName());
        lqw.like(StringUtils.isNotBlank(adminSmsTemplate.getName()),AdminSmsTemplate::getName ,adminSmsTemplate.getName());
        lqw.like(StringUtils.isNotBlank(adminSmsTemplate.getTitle()),AdminSmsTemplate::getTitle ,adminSmsTemplate.getTitle());
        lqw.like(StringUtils.isNotBlank(adminSmsTemplate.getTemplateCode()),AdminSmsTemplate::getTemplateCode ,adminSmsTemplate.getTemplateCode());
        lqw.like(StringUtils.isNotBlank(adminSmsTemplate.getSignName()),AdminSmsTemplate::getSignName ,adminSmsTemplate.getSignName());
        lqw.ge(betweenCreatedTimeRequest!=null && betweenCreatedTimeRequest.getStartTime() != null, AdminSmsTemplate::getCreatedTime, betweenCreatedTimeRequest.getStartTime());
        lqw.lt(betweenCreatedTimeRequest!=null && betweenCreatedTimeRequest.getEndTime() != null, AdminSmsTemplate::getCreatedTime, betweenCreatedTimeRequest.getEndTime());
        lqw.eq(adminSmsTemplate.getUpdatedTime() != null, AdminSmsTemplate::getUpdatedTime ,adminSmsTemplate.getUpdatedTime());
        List<AdminSmsTemplate> list = adminSmsTemplateService.list(lqw);
        return R.success(new PageSerializable<>(list));
    }

    /**
     * 获取短信模板详细信息
     */
    @GetMapping(value = "/{id}" )
    @Node(value = "adminSmsTemplate::getInfo", name = "短信模板;;详细信息")
    public R<AdminSmsTemplate> getInfo(@PathVariable("id" ) Long id) {
        return R.success(adminSmsTemplateService.getById(id));
    }

    /**
     * 新增短信模板
     */
    @PostMapping
    @Node(value = "adminSmsTemplate::add", name = "短信模板;;新增")
    public R<Boolean> add(@RequestBody AdminSmsTemplate adminSmsTemplate) {
        return R.success(adminSmsTemplateService.save(adminSmsTemplate));
    }

    /**
     * 修改短信模板
     */
    @PutMapping
    @Node(value = "adminSmsTemplate::edit", name = "短信模板;;修改")
    public R<Boolean> edit(@RequestBody AdminSmsTemplate adminSmsTemplate) {
        return R.success(adminSmsTemplateService.updateById(adminSmsTemplate));
    }

    /**
     * 删除短信模板
     */
    @DeleteMapping("/{ids}" )
    @Node(value = "adminSmsTemplate::remove", name = "短信模板;;删除")
    public R<Boolean> remove(@PathVariable Long[] ids) {
        return R.success(adminSmsTemplateService.removeByIds(Arrays.asList(ids)));
    }
}