package work.soho.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.admin.domain.AdminEmailTemplate;

public interface AdminEmailTemplateMapper extends BaseMapper<AdminEmailTemplate> {

}