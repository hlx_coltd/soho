package work.soho.groovy.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.groovy.biz.domain.GroovyInfo;

public interface GroovyInfoMapper extends BaseMapper<GroovyInfo> {

}