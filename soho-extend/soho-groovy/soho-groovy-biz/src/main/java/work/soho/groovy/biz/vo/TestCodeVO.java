package work.soho.groovy.biz.vo;

import lombok.Data;

@Data
public class TestCodeVO {
    private String code;
    private String testCode;
}
