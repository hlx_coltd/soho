package work.soho.groovy.biz.service;

import work.soho.groovy.biz.domain.GroovyGroup;
import com.baomidou.mybatisplus.extension.service.IService;

public interface GroovyGroupService extends IService<GroovyGroup> {

}