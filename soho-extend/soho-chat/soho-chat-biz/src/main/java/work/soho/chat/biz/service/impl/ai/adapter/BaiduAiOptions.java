package work.soho.chat.biz.service.impl.ai.adapter;

import lombok.Data;

@Data
public class BaiduAiOptions {
    private String apiKey;

    private String secretKey;
}
