package work.soho.chat.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.chat.biz.domain.ChatUserFriendQuestions;

public interface ChatUserFriendQuestionsMapper extends BaseMapper<ChatUserFriendQuestions> {

}