package work.soho.chat.biz.service;

import work.soho.chat.biz.domain.ChatGroupQuestions;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ChatGroupQuestionsService extends IService<ChatGroupQuestions> {

}