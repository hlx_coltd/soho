package work.soho.chat.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.chat.biz.domain.ChatUserNotice;

public interface ChatUserNoticeMapper extends BaseMapper<ChatUserNotice> {

}