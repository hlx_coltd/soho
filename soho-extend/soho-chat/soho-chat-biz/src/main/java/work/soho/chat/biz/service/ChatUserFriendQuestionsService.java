package work.soho.chat.biz.service;

import work.soho.chat.biz.domain.ChatUserFriendQuestions;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ChatUserFriendQuestionsService extends IService<ChatUserFriendQuestions> {

}