package work.soho.chat.biz.vo;

import lombok.Data;

/**
 * 创建会话
 */
@Data
public class CustomerServiceSessionVO {
    /**
     * 会话ID
     */
    private Long sessionId;
}
