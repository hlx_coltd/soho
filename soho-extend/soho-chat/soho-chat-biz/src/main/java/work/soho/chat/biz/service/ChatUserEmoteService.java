package work.soho.chat.biz.service;

import work.soho.chat.biz.domain.ChatUserEmote;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ChatUserEmoteService extends IService<ChatUserEmote> {

}