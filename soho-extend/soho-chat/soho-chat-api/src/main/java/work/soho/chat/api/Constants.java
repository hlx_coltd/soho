package work.soho.chat.api;

public class Constants {
    /**
     * 聊天用户角色名
     */
    public static final String ROLE_NAME = "chat";

    /**
     * 原始类型
     */
    public static final String ORIGIN_ROLE_GUEST = "guest";
}
