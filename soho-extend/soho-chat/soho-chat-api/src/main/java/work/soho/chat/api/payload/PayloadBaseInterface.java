package work.soho.chat.api.payload;

public interface PayloadBaseInterface {
    public String getId();
    public String getType();
}
