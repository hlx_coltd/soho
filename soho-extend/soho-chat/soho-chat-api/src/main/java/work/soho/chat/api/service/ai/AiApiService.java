package work.soho.chat.api.service.ai;

public interface AiApiService {
    String query(String question);
}
