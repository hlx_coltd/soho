package work.soho.chat.api.service;

public interface QuestionService {
    String ask(String uid, String q);
}
