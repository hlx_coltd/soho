package work.soho.open.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.open.biz.domain.OpenApp;

public interface OpenAppMapper extends BaseMapper<OpenApp> {

}