package work.soho.open.biz.service;

import work.soho.open.biz.domain.OpenCode;
import com.baomidou.mybatisplus.extension.service.IService;

public interface OpenCodeService extends IService<OpenCode> {

}