package work.soho.open.biz.service;

import work.soho.open.biz.domain.OpenApp;
import com.baomidou.mybatisplus.extension.service.IService;

public interface OpenAppService extends IService<OpenApp> {

}