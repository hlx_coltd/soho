package work.soho.open.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.open.biz.domain.OpenCode;

public interface OpenCodeMapper extends BaseMapper<OpenCode> {

}