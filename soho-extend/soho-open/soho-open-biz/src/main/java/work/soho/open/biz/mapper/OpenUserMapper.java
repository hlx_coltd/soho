package work.soho.open.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.open.biz.domain.OpenUser;

public interface OpenUserMapper extends BaseMapper<OpenUser> {

}