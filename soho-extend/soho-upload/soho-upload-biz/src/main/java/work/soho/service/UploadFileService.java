package work.soho.service;

import work.soho.domain.UploadFile;
import com.baomidou.mybatisplus.extension.service.IService;

public interface UploadFileService extends IService<UploadFile> {

}