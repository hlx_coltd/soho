package work.soho.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import work.soho.domain.UploadFile;

public interface UploadFileMapper extends BaseMapper<UploadFile> {

}